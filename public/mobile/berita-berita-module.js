(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["berita-berita-module"],{

/***/ "./src/app/berita/berita.module.ts":
/*!*****************************************!*\
  !*** ./src/app/berita/berita.module.ts ***!
  \*****************************************/
/*! exports provided: BeritaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeritaPageModule", function() { return BeritaPageModule; });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _berita_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./berita.page */ "./src/app/berita/berita.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var BeritaPageModule = /** @class */ (function () {
    function BeritaPageModule() {
    }
    BeritaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild([{ path: '', component: _berita_page__WEBPACK_IMPORTED_MODULE_5__["BeritaPage"] }])
            ],
            declarations: [_berita_page__WEBPACK_IMPORTED_MODULE_5__["BeritaPage"]]
        })
    ], BeritaPageModule);
    return BeritaPageModule;
}());



/***/ }),

/***/ "./src/app/berita/berita.page.html":
/*!*****************************************!*\
  !*** ./src/app/berita/berita.page.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button  (click)=\"gotoHome()\">\n          <ion-icon name=\"arrow-round-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      Berita\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg\">\n\n  <ion-item class=\"list-item\" *ngFor=\"let brt of berita | async\">\n    <ion-row justify-content-center align-items-center class=\"list-item\">\n      <ion-col size=\"3\" align-self=\"center\" > \n        <ion-img src=\"assets/images/berita.jpg\"></ion-img>\n      </ion-col>  \n      <ion-col size=\"9\">   \n        <ion-text class=\"berita-judul\">{{brt.judul}}</ion-text> <br/> \n        <ion-text class=\"berita-tanggal\">{{brt.tanggal}}</ion-text > <br/> \n        <ion-text class=\"berita-isi\">{{brt.isi}}</ion-text > <br/> \n      </ion-col>  \n    </ion-row>\n  </ion-item>\n \n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/berita/berita.page.scss":
/*!*****************************************!*\
  !*** ./src/app/berita/berita.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bg {\n  background-color: lightgray;\n  padding: 5px; }\n\n.berita-judul {\n  font-size: 12pt;\n  font-weight: bold; }\n\n.berita-tanggal {\n  font-size: 8pt;\n  color: darkblue; }\n\n.berita-isi {\n  font-size: 8pt; }\n\n.list-item {\n  margin: 5px;\n  background-color: white; }\n\n.list-item:hover {\n  cursor: pointer;\n  background-color: lightgray; }\n\n.list-item:focus {\n  cursor: pointer;\n  background-color: lightslategrey; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmVyaXRhL0Q6XFxTRVJWRVJcXG5vZGVqc1Byb2plY3RcXE1PTklUT1JJTkctU1VBUkEtUElMUFJFU1xcbW9uaXRvcmluZy1zdWFyYS1waWxwcmVzLW1vYmlsZS9zcmNcXGFwcFxcYmVyaXRhXFxiZXJpdGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNEJBQTJCO0VBQzNCLGFBQVksRUFDZjs7QUFFRDtFQUNJLGdCQUFlO0VBQ2Ysa0JBQWlCLEVBQ3BCOztBQUNEO0VBQ0ksZUFBYztFQUNkLGdCQUFlLEVBQ2xCOztBQUNEO0VBQ0ksZUFBYyxFQUNqQjs7QUFHRDtFQUNJLFlBQVc7RUFDWCx3QkFBdUIsRUFDMUI7O0FBRUQ7RUFDSSxnQkFBZTtFQUNmLDRCQUEyQixFQUM5Qjs7QUFFRDtFQUNJLGdCQUFlO0VBQ2YsaUNBQWdDLEVBQ25DIiwiZmlsZSI6InNyYy9hcHAvYmVyaXRhL2Jlcml0YS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmd7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xuICAgIHBhZGRpbmc6IDVweDtcbn1cblxuLmJlcml0YS1qdWR1bHtcbiAgICBmb250LXNpemU6IDEycHQ7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uYmVyaXRhLXRhbmdnYWx7XG4gICAgZm9udC1zaXplOiA4cHQ7XG4gICAgY29sb3I6IGRhcmtibHVlO1xufVxuLmJlcml0YS1pc2l7XG4gICAgZm9udC1zaXplOiA4cHQ7XG59XG5cblxuLmxpc3QtaXRlbSB7IFxuICAgIG1hcmdpbjogNXB4OyBcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbiAgXG4ubGlzdC1pdGVtOmhvdmVyIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7IFxuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JheTtcbn1cbiAgXG4ubGlzdC1pdGVtOmZvY3VzIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7IFxuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0c2xhdGVncmV5O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/berita/berita.page.ts":
/*!***************************************!*\
  !*** ./src/app/berita/berita.page.ts ***!
  \***************************************/
/*! exports provided: BeritaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeritaPage", function() { return BeritaPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BeritaPage = /** @class */ (function () {
    function BeritaPage(router, db) {
        this.router = router;
        this.db = db;
        this.loadBerita();
    }
    BeritaPage.prototype.gotoHome = function () {
        console.log('home');
        this.router.navigate(['/tabs/home']);
    };
    BeritaPage.prototype.loadBerita = function () {
        //  get Data
        this.berita = this.db.collection('/test_berita').valueChanges();
    };
    BeritaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-berita',
            template: __webpack_require__(/*! ./berita.page.html */ "./src/app/berita/berita.page.html"),
            styles: [__webpack_require__(/*! ./berita.page.scss */ "./src/app/berita/berita.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], BeritaPage);
    return BeritaPage;
}());



/***/ })

}]);
//# sourceMappingURL=berita-berita-module.js.map