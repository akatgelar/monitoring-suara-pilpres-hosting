(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild([{ path: '', component: _home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"] }])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.html":
/*!*************************************!*\
  !*** ./src/app/home/home.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Home\n    </ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n    <ion-img class=\"img-banner\" src=\"assets/images/banner.png\"></ion-img>\n    \n    <ion-card class=\"card-1\">\n\n      <ion-row>\n        <ion-col size=\"3\"> \n          <ion-img src=\"assets/images/user.png\"></ion-img>\n        </ion-col>  \n        <ion-col size=\"1\"> \n        </ion-col>  \n        <ion-col size=\"8\">  \n          <ion-card-title>{{str_nama}}</ion-card-title>\n          <ion-label>No HP : {{str_no_hp}}</ion-label> <br/>\n          <ion-label>Kode TPS : {{str_kode_tps}}</ion-label> \n        </ion-col> \n      </ion-row>\n\n      \n      <ion-row >\n        <ion-col size=\"3\"> \n          <ion-img src=\"assets/images/tps.png\"></ion-img>\n        </ion-col>  \n        <ion-col size=\"1\"> \n        </ion-col>  \n        <ion-col size=\"8\">  \n          <ion-card-title>{{str_nama_tps}}</ion-card-title>\n          <ion-label position=\"fixed\">Kel {{str_nama_kelurahan}}, </ion-label> <br/> \n          <ion-label position=\"fixed\">Kec {{str_nama_kecamatan}}, </ion-label> <br/> \n          <ion-label position=\"fixed\">{{str_nama_kota}}, </ion-label>  <br/> \n          <ion-label position=\"fixed\">Prov {{str_nama_provinsi}}</ion-label> <br/> \n        </ion-col> \n      </ion-row> \n\n    </ion-card>\n\n    \n    <ion-row>\n      <ion-col size=\"6\"> \n        <ion-card [ngClass]=\"{'card-button': status_absen==false, 'card-button-disabled': status_absen==true}\" (click)=\"gotoAbsen()\">\n          <ion-row justify-content-center align-items-center> \n            <ion-col size=\"4\"> \n              <ion-img src=\"assets/images/star_full.png\"></ion-img>\n            </ion-col>    \n            <ion-col size=\"8\">  \n              <ion-card-title class=\"title\">Absen</ion-card-title> \n            </ion-col> \n          </ion-row>\n        </ion-card>\n      </ion-col> \n      <ion-col size=\"6\"> \n        <ion-card [ngClass]=\"{'card-button': status_kirim==false, 'card-button-disabled': status_kirim==true}\" (click)=\"gotoKirim()\">\n          <ion-row justify-content-center align-items-center> \n            <ion-col size=\"4\"> \n              <ion-img src=\"assets/images/mail_send.png\"></ion-img>\n            </ion-col>    \n            <ion-col size=\"8\">  \n              <ion-card-title class=\"title\">Kirim Suara</ion-card-title> \n            </ion-col> \n          </ion-row>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    \n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img-banner {\n  max-height: 45vh;\n  overflow: hidden; }\n\n.card-1 {\n  padding-top: 5px;\n  padding-bottom: 5px; }\n\n.title {\n  font-size: 12pt; }\n\n.card-button {\n  padding-top: 5px;\n  padding-bottom: 5px; }\n\n.card-button:hover {\n  cursor: pointer;\n  background-color: lightgray; }\n\n.card-button:focus {\n  cursor: pointer;\n  background-color: lightslategrey; }\n\n.card-button-disabled {\n  background-color: #d5dae0;\n  cursor: default; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9EOlxcU0VSVkVSXFxub2RlanNQcm9qZWN0XFxNT05JVE9SSU5HLVNVQVJBLVBJTFBSRVNcXG1vbml0b3Jpbmctc3VhcmEtcGlscHJlcy1tb2JpbGUvc3JjXFxhcHBcXGhvbWVcXGhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWdCO0VBQ2hCLGlCQUFnQixFQUNqQjs7QUFFRDtFQUNFLGlCQUFnQjtFQUNoQixvQkFBbUIsRUFDcEI7O0FBRUQ7RUFDRSxnQkFBZSxFQUNoQjs7QUFFRDtFQUNFLGlCQUFnQjtFQUNoQixvQkFBbUIsRUFDcEI7O0FBRUQ7RUFDRSxnQkFBZTtFQUNmLDRCQUEyQixFQUM1Qjs7QUFFRDtFQUNFLGdCQUFlO0VBQ2YsaUNBQWdDLEVBQ2pDOztBQUVEO0VBQ0UsMEJBQXlCO0VBQ3pCLGdCQUFlLEVBQ2hCIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWctYmFubmVyIHtcbiAgbWF4LWhlaWdodDogNDV2aDsgXG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5jYXJkLTEge1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufSBcbiBcbi50aXRsZXtcbiAgZm9udC1zaXplOiAxMnB0OyBcbn1cblxuLmNhcmQtYnV0dG9uIHsgXG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG5cbi5jYXJkLWJ1dHRvbjpob3ZlciB7XG4gIGN1cnNvcjogcG9pbnRlcjsgXG4gIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JheTtcbn1cblxuLmNhcmQtYnV0dG9uOmZvY3VzIHtcbiAgY3Vyc29yOiBwb2ludGVyOyBcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRzbGF0ZWdyZXk7XG59XG5cbi5jYXJkLWJ1dHRvbi1kaXNhYmxlZCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNkNWRhZTA7XG4gIGN1cnNvcjogZGVmYXVsdDsgXG59Il19 */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_session_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/session.service */ "./src/app/services/session.service.ts");
/* harmony import */ var _services_error_message_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/error-message.service */ "./src/app/services/error-message.service.ts");
/* harmony import */ var _services_http_request_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/http-request.service */ "./src/app/services/http-request.service.ts");
/* harmony import */ var _services_converter_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/converter.service */ "./src/app/services/converter.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomePage = /** @class */ (function () {
    function HomePage(router, session, httpRequest, errorMessage, converter, loadingController, toastController) {
        this.router = router;
        this.session = session;
        this.httpRequest = httpRequest;
        this.errorMessage = errorMessage;
        this.converter = converter;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.str_nama = '';
        this.str_no_hp = '';
        this.str_kode_tps = '';
        this.str_nama_provinsi = '';
        this.str_nama_kota = '';
        this.str_nama_kecamatan = '';
        this.str_nama_kelurahan = '';
        this.str_nama_tps = '';
        this.status_absen = false;
        this.status_kirim = false;
        var data = JSON.parse(this.session.getData());
        this.str_nama = data['nama'];
        this.str_no_hp = data['no_hp'];
        this.str_kode_tps = data['kode_tps'];
        this.str_nama_provinsi = this.capitalize(data['nama_provinsi']);
        this.str_nama_kota = this.capitalize(data['nama_kota']);
        this.str_nama_kecamatan = this.capitalize(data['nama_kecamatan']);
        this.str_nama_kelurahan = this.capitalize(data['nama_kelurahan']);
        this.str_nama_tps = data['nama_tps'];
        this.status_absen = data['status_absen'];
        this.status_kirim = data['status_kirim'];
        // console.log(this.status_absen);
        // console.log(this.status_kirim);
        this.session.getData();
        this.session.getToken();
    }
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_6__["NavigationEnd"]) {
                var data = JSON.parse(_this.session.getData());
                _this.status_absen = data['status_absen'];
                _this.status_kirim = data['status_kirim'];
                // console.log(this.status_absen);
                // console.log(this.status_kirim);
            }
        });
    };
    HomePage.prototype.gotoAbsen = function () {
        if (this.status_absen !== true) {
            this.router.navigate(['/absen']);
        }
    };
    HomePage.prototype.gotoKirim = function () {
        if (this.status_kirim !== true) {
            this.router.navigate(['/kirim']);
        }
    };
    HomePage.prototype.capitalize = function (str) {
        var arr = str.split(' ');
        var res = '';
        arr.forEach(function (a) {
            var b = a.toLowerCase();
            var c = b.charAt(0).toUpperCase() + b.slice(1);
            res = res + c + ' ';
        });
        res = res.substring(0, res.length - 1);
        return res;
    };
    HomePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.page.html */ "./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _services_session_service__WEBPACK_IMPORTED_MODULE_1__["SessionService"],
            _services_http_request_service__WEBPACK_IMPORTED_MODULE_3__["HttpRequestService"],
            _services_error_message_service__WEBPACK_IMPORTED_MODULE_2__["ErrorMessageService"],
            _services_converter_service__WEBPACK_IMPORTED_MODULE_4__["ConverterService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map