(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ProfilePageModule = /** @class */ (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild([{ path: '', component: _profile_page__WEBPACK_IMPORTED_MODULE_5__["ProfilePage"] }])
            ],
            declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_5__["ProfilePage"]]
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());



/***/ }),

/***/ "./src/app/profile/profile.page.html":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-button  (click)=\"gotoHome()\">\n          <ion-icon name=\"arrow-round-back\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>\n      Profile\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-row justify-content-center align-items-center>\n    <ion-col size=\"4\"></ion-col>   \n    <ion-col size=\"4\"> \n      <ion-img src=\"assets/images/user.png\"></ion-img>\n    </ion-col>    \n    <ion-col size=\"4\"></ion-col>  \n  </ion-row>\n\n  <ion-row justify-content-center align-items-center>\n    <ion-col size=\"3\"></ion-col>   \n    <ion-col size=\"6\"> \n      <!-- <ion-button expand=\"full\" shape=\"round\" (click)=\"updateFoto()\">Ganti Foto Profil</ion-button> -->\n    </ion-col>    \n    <ion-col size=\"3\"></ion-col>  \n  </ion-row>\n  \n  <ion-row>  \n    <ion-col size=\"12\"> \n\n      <br/>\n      <br/>\n      <ion-item>\n        <ion-label position=\"stacked\">Provinsi</ion-label>\n        <ion-input class=\"input-box\" placeholder=\"Masukkan Provinsi\" type=\"text\" value=\"{{str_nama_provinsi}}\" disabled=\"true\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Kota/Kab</ion-label>\n        <ion-input class=\"input-box\" placeholder=\"Masukkan Kota/Kab\" type=\"text\" value=\"{{str_nama_kota}}\" disabled=\"true\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Kecamatan</ion-label>\n        <ion-input class=\"input-box\" placeholder=\"Masukkan Kecamatan\" type=\"text\" value=\"{{str_nama_kecamatan}}\" disabled=\"true\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Kelurahan</ion-label>\n        <ion-input class=\"input-box\" placeholder=\"Masukkan Kelurahan\" type=\"text\" value=\"{{str_nama_kelurahan}}\" disabled=\"true\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">TPS</ion-label>\n        <ion-input class=\"input-box\" placeholder=\"Masukkan TPS\" type=\"text\" value=\"{{str_nama_tps}}\" disabled=\"true\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Kode TPS</ion-label>\n        <ion-input class=\"input-box\" placeholder=\"Masukkan Kode TPS\" type=\"text\" value=\"{{str_kode_tps}}\" disabled=\"true\"></ion-input>\n      </ion-item>\n\n      <br/>\n      <br/>\n      <form [formGroup]=\"passwordForm\">\n      <ion-item>\n        <ion-label position=\"stacked\">Username</ion-label>\n        <ion-input formControlName=\"username\" class=\"input-box\" placeholder=\"Masukkan Username\" type=\"text\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Password</ion-label>\n        <ion-input formControlName=\"password\" class=\"input-box\" placeholder=\"Masukkan Password\" type=\"password\" value=\"\"></ion-input>\n      </ion-item> \n      <ion-button expand=\"full\" shape=\"round\" [disabled]=\"!passwordForm.valid\" (click)=\"updatePassword()\">Ganti Password</ion-button>\n      </form>\n\n      <br/>\n      <br/>\n      <form [formGroup]=\"profileForm\">\n      <ion-item>\n        <ion-label position=\"stacked\">Nama</ion-label>\n        <ion-input formControlName=\"nama\" class=\"input-box\" placeholder=\"Masukkan Nama\" type=\"text\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">No HP</ion-label>\n        <ion-input formControlName=\"no_hp\" class=\"input-box\" placeholder=\"Masukkan No HP\" type=\"text\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Tempat Lahir</ion-label>\n        <ion-input formControlName=\"tempat_lahir\" class=\"input-box\" placeholder=\"Masukkan Tempat Lahir\" type=\"text\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Tanggal Lahir</ion-label>\n        <ion-input formControlName=\"tanggal_lahir\" class=\"input-box\" placeholder=\"Masukkan Tanggal Lahir\" type=\"text\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">Alamat</ion-label>\n        <ion-input formControlName=\"alamat\" class=\"input-box\" placeholder=\"Masukkan Alamat\" type=\"text\"></ion-input>\n      </ion-item>\n      <ion-button expand=\"full\" shape=\"round\" [disabled]=\"!profileForm.valid\" (click)=\"updateProfile()\">Update Data Diri</ion-button>\n      </form>\n      <br/>\n      <br/>\n      <br/>\n      <br/>\n      <ion-button expand=\"full\" color=\"danger\" shape=\"round\" (click)=\"logout()\">Logout</ion-button>\n\n    </ion-col> \n  </ion-row>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/profile/profile.page.scss":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/profile/profile.page.ts":
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_session_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/session.service */ "./src/app/services/session.service.ts");
/* harmony import */ var _services_error_message_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/error-message.service */ "./src/app/services/error-message.service.ts");
/* harmony import */ var _services_http_request_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/http-request.service */ "./src/app/services/http-request.service.ts");
/* harmony import */ var _services_converter_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/converter.service */ "./src/app/services/converter.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var ProfilePage = /** @class */ (function () {
    function ProfilePage(router, session, httpRequest, errorMessage, converter, loadingController, toastController) {
        this.router = router;
        this.session = session;
        this.httpRequest = httpRequest;
        this.errorMessage = errorMessage;
        this.converter = converter;
        this.loadingController = loadingController;
        this.toastController = toastController;
        // updateUserUrl = 'http://us-central1-monitoring-suara-pilpres-2019.cloudfunctions.net/updateUser';
        this.updateUserUrl = '/api/updateUser';
        this.str_nama = '';
        this.str_no_hp = '';
        this.str_tempat_lahir = '';
        this.str_tanggal_lahir = '';
        this.str_alamat = '';
        this.str_username = '';
        this.str_kode_tps = '';
        this.str_nama_provinsi = '';
        this.str_nama_kota = '';
        this.str_nama_kecamatan = '';
        this.str_nama_kelurahan = '';
        this.str_nama_tps = '';
        var data = JSON.parse(this.session.getData());
        this.str_nama = data['nama'];
        this.str_no_hp = data['no_hp'];
        this.str_tempat_lahir = data['tempat_lahir'];
        this.str_tanggal_lahir = data['tanggal_lahir'];
        this.str_alamat = data['alamat'];
        this.str_username = data['username'];
        this.str_kode_tps = data['kode_tps'];
        this.str_nama_provinsi = data['nama_provinsi'];
        this.str_nama_kota = data['nama_kota'];
        this.str_nama_kecamatan = data['nama_kecamatan'];
        this.str_nama_kelurahan = data['nama_kelurahan'];
        this.str_nama_tps = data['nama_tps'];
        var txt_username = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        var txt_password = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.passwordForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            username: txt_username,
            password: txt_password
        });
        this.passwordForm.setValue({
            username: this.str_username,
            password: ''
        });
        var txt_nama = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        var txt_alamat = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        var txt_no_hp = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        var txt_tempat_lahir = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        var txt_tanggal_lahir = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            nama: txt_nama,
            alamat: txt_alamat,
            no_hp: txt_no_hp,
            tempat_lahir: txt_tempat_lahir,
            tanggal_lahir: txt_tanggal_lahir
        });
        this.profileForm.setValue({
            nama: this.str_nama,
            alamat: this.str_alamat,
            no_hp: this.str_no_hp,
            tempat_lahir: this.str_tempat_lahir,
            tanggal_lahir: this.str_tanggal_lahir
        });
    }
    ProfilePage.prototype.updateFoto = function () {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            color: 'success',
                            position: 'top',
                            message: 'Foto profil berhasil diupdate.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updatePassword = function () {
        return __awaiter(this, void 0, void 0, function () {
            var passwordVal, passwordJson, loading;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        passwordVal = this.passwordForm.value;
                        passwordVal['id'] = JSON.parse(this.session.getData())['kode_tps'];
                        passwordJson = JSON.stringify(passwordVal);
                        console.log(this.updateUserUrl);
                        console.log(passwordJson);
                        return [4 /*yield*/, this.loadingController.create({
                                message: 'Loading...',
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        // call post login
                        this.httpRequest.httpPost(this.updateUserUrl, passwordJson).subscribe(
                        // if ok
                        function (result) { return __awaiter(_this, void 0, void 0, function () {
                            var new_data, toast, toast;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log(result);
                                        if (!(result['status'] === true)) return [3 /*break*/, 4];
                                        new_data = result['result'];
                                        new_data['iat'] = JSON.parse(this.session.getData())['iat'];
                                        new_data['exp'] = JSON.parse(this.session.getData())['exp'];
                                        this.session.setData(new_data);
                                        // message
                                        return [4 /*yield*/, loading.dismiss()];
                                    case 1:
                                        // message
                                        _a.sent();
                                        return [4 /*yield*/, this.toastController.create({
                                                message: result['message'],
                                                duration: 2000
                                            })];
                                    case 2:
                                        toast = _a.sent();
                                        return [4 /*yield*/, toast.present()];
                                    case 3:
                                        _a.sent();
                                        // redirect
                                        this.router.navigate(['/tabs/home']);
                                        return [3 /*break*/, 8];
                                    case 4: 
                                    // message
                                    return [4 /*yield*/, loading.dismiss()];
                                    case 5:
                                        // message
                                        _a.sent();
                                        return [4 /*yield*/, this.toastController.create({
                                                message: result['message'],
                                                duration: 2000
                                            })];
                                    case 6:
                                        toast = _a.sent();
                                        return [4 /*yield*/, toast.present()];
                                    case 7:
                                        _a.sent();
                                        _a.label = 8;
                                    case 8: return [2 /*return*/];
                                }
                            });
                        }); }, 
                        // if error
                        function (error) { return __awaiter(_this, void 0, void 0, function () {
                            var toast;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log('error');
                                        console.log(error);
                                        // message
                                        return [4 /*yield*/, loading.dismiss()];
                                    case 1:
                                        // message
                                        _a.sent();
                                        return [4 /*yield*/, this.toastController.create({
                                                message: error.message,
                                                duration: 2000
                                            })];
                                    case 2:
                                        toast = _a.sent();
                                        return [4 /*yield*/, toast.present()];
                                    case 3:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.updateProfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var profileVal, profileJson, loading;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        profileVal = this.profileForm.value;
                        profileVal['id'] = JSON.parse(this.session.getData())['kode_tps'];
                        profileJson = JSON.stringify(profileVal);
                        console.log(this.updateUserUrl);
                        console.log(profileJson);
                        return [4 /*yield*/, this.loadingController.create({
                                message: 'Loading...',
                            })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        // call post login
                        this.httpRequest.httpPost(this.updateUserUrl, profileJson).subscribe(
                        // if ok
                        function (result) { return __awaiter(_this, void 0, void 0, function () {
                            var new_data, toast, toast;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log(result);
                                        if (!(result['status'] === true)) return [3 /*break*/, 4];
                                        new_data = result['result'];
                                        new_data['iat'] = JSON.parse(this.session.getData())['iat'];
                                        new_data['exp'] = JSON.parse(this.session.getData())['exp'];
                                        this.session.setData(new_data);
                                        // message
                                        return [4 /*yield*/, loading.dismiss()];
                                    case 1:
                                        // message
                                        _a.sent();
                                        return [4 /*yield*/, this.toastController.create({
                                                message: result['message'],
                                                duration: 2000
                                            })];
                                    case 2:
                                        toast = _a.sent();
                                        return [4 /*yield*/, toast.present()];
                                    case 3:
                                        _a.sent();
                                        // redirect
                                        this.router.navigate(['/tabs/home']);
                                        return [3 /*break*/, 8];
                                    case 4: 
                                    // message
                                    return [4 /*yield*/, loading.dismiss()];
                                    case 5:
                                        // message
                                        _a.sent();
                                        return [4 /*yield*/, this.toastController.create({
                                                message: result['message'],
                                                duration: 2000
                                            })];
                                    case 6:
                                        toast = _a.sent();
                                        return [4 /*yield*/, toast.present()];
                                    case 7:
                                        _a.sent();
                                        _a.label = 8;
                                    case 8: return [2 /*return*/];
                                }
                            });
                        }); }, 
                        // if error
                        function (error) { return __awaiter(_this, void 0, void 0, function () {
                            var toast;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        console.log('error');
                                        console.log(error);
                                        // message
                                        return [4 /*yield*/, loading.dismiss()];
                                    case 1:
                                        // message
                                        _a.sent();
                                        return [4 /*yield*/, this.toastController.create({
                                                message: error.message,
                                                duration: 2000
                                            })];
                                    case 2:
                                        toast = _a.sent();
                                        return [4 /*yield*/, toast.present()];
                                    case 3:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.logout = function () {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.session.logOut();
                        return [4 /*yield*/, this.toastController.create({
                                // color: 'success',
                                // position: 'top',
                                message: 'Logout sukses.',
                                duration: 2000
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        this.router.navigate(['/login']);
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.gotoHome = function () {
        console.log('home');
        this.router.navigate(['/tabs/home']);
    };
    ProfilePage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-profile',
            template: __webpack_require__(/*! ./profile.page.html */ "./src/app/profile/profile.page.html"),
            styles: [__webpack_require__(/*! ./profile.page.scss */ "./src/app/profile/profile.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_session_service__WEBPACK_IMPORTED_MODULE_3__["SessionService"],
            _services_http_request_service__WEBPACK_IMPORTED_MODULE_5__["HttpRequestService"],
            _services_error_message_service__WEBPACK_IMPORTED_MODULE_4__["ErrorMessageService"],
            _services_converter_service__WEBPACK_IMPORTED_MODULE_6__["ConverterService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]])
    ], ProfilePage);
    return ProfilePage;
}());



/***/ })

}]);
//# sourceMappingURL=profile-profile-module.js.map