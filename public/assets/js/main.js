/************* Main Js File ************************
    Template Name: Atoz- App Landing Template
    Author: Tanmoy Dhar
    Version: 1.0
    Copyright 2018
****************************************/


/*==================================

        Table of Content

        1. Window Load Function
            a. Preloader Setup
            b. Portfolio Isotope Setup
        2. Document Ready Function
            a. ScrollIt Setup
            b. Navbar Scrolling Background
            c. Stats Counter Setup
            d. Navbar Close On Click Mobile Responsive
            e. Stellar Setup
            f. Magnific Popup Setup
            g. Blog OwlCarousel Setup
            h. Testimonial OwlCarousel Setup
            i. Contact Form Setup

==================================*/

    /*========Window Load Function========*/
    $(window).on("load", function() {
        "use strict";

 /* smooth scroll
  -------------------------------------------------------*/
      $.scrollIt({

        easing: 'swing',      // the easing function for animation
        scrollTime: 900,       // how long (in ms) the animation takes
        activeClass: 'active', // class given to the active nav element
        onPageChange: null,    // function(pageIndex) that is called when page is changed
        topOffset: -70,
        upKey: 38,                // key code to navigate to the next section
        downKey: 40          // key code to navigate to the previous section

      });
    
     var win = $(window);
           
  
    win.on("scroll", function () {
      var wScrollTop  = $(window).scrollTop();    
        
        if (wScrollTop > 100) {
            $(".navbar").addClass("navbar-colored");
        } else {
            $(".navbar").removeClass("navbar-colored");
        }
    });

    /* close navbar-collapse when a  clicked */
    $(".navbar-nav a").not('.dropdown-toggle').on('click', function () {
        $(".navbar-collapse").removeClass("show");
    });
    
    /* close navbar-collapse when a  clicked */
    $('.dropdown').on('click', function () {
        $(this).toggleClass("show");
    });
    $("#client-caro").owlCarousel({
      navigation: false,
      pagination: true,
      slideSpeed: 800,
      paginationSpeed: 800,
      smartSpeed: 500,
      autoplay: true,
      singleItem: true,
      loop: true,
      responsive:{
        0:{
          items:1
        },
        680:{
          items:1
        },
        1000:{
          items:2
        }
      }
    });
  $("#c-caro").owlCarousel({
      navigation:  true,
      pagination:false,
      slideSpeed: 800,
      nav:true,
      paginationSpeed: 800,
      smartSpeed: 500,
      autoplay: false,
      singleItem: true,
      navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
      loop: true,
      responsive:{
        0:{
          items:1
        },
        680:{
          items:1
        },
        1000:{
          items:2
        }
      }
    });
    $(".client-caro").owlCarousel({
            autoplay: true,
        loop: true,
        margin:25,
        dots:false,
        slideTransition:'linear',
        autoplayTimeout:4500,
        autoplayHoverPause:true,
        autoplaySpeed:4500,
        responsive:{
            0:{
                items:2
            },
            500: {
                items:3
            },
            600:{
                items:3
            },
            800:{
                items:4
            },
            1200:{
                items:5
            }
      }
    });


         /*---------------------------------------------------
            pricing plan swither
        ----------------------------------------------------*/
        var e = document.getElementById("pricing-table-monthly"),
            d = document.getElementById("pricing-table-yearly"),
            t = document.getElementById("switcher"),
            m = document.getElementById("monthly-section"),
            y = document.getElementById("yearly-section");

        $('#pricing-table-monthly').on('click', function () {
            t.checked = false;
            e.classList.add("toggler-pircing-is-active");
            d.classList.remove("toggler-pircing-is-active");
            m.classList.remove("inactive");
            y.classList.add("inactive");
        });

        $('#pricing-table-yearly').on('click', function () {
            t.checked = true;
            e.classList.add("toggler-pircing-is-active");
            d.classList.remove("toggler-pircing-is-active");
            m.classList.remove("inactive");
            y.classList.add("inactive");
        });

        $('#switcher-item').on('click', function () {
            d.classList.toggle("toggler-pircing-is-active");
            e.classList.toggle("toggler-pircing-is-active");
            m.classList.toggle("inactive");
            y.classList.toggle("inactive");
        });
        

    $("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      items:1,
      nav:true,
      loop:true,
      dots:false,
      autoplay:true,
      navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
 
  }); 
    // caro not use


        // welcome banner slider js
    $(".header-slides").owlCarousel({
        loop: !0,
        responsiveClass: !0,
        items: 1,
        nav:true,
        dots: !0,
        autoplay: !0,
        margin: 20,
        animateOut: "bounceOutRight",
        animateIn: "bounceInLeft",
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
    });
    $(".left-header-slider").owlCarousel({
        loop: !0,
        responsiveClass: !0,
        items: 1,
        nav:true,
        dots: !0,
        autoplay: !1,
        margin: 20,
        animateOut: "bounceOutRight",
        animateIn: "bounceInLeft",
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
    });

    function appScreenshotCarousel () {
        if ($('.app-section-slider').length) {

            var swiper = new Swiper('.app-section-slider', {
                effect: 'coverflow',
                loop: true,
                centeredSlides: true,
                slidesPerView: 4,
                initialSlide: 4,
                keyboardControl: true,
                mousewheelControl: false,
                lazyLoading: true,
                preventClicks: false,
                preventClicksPropagation: false,
                lazyLoadingInPrevNext: true,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                coverflow: {
                    rotate: 0,
                    stretch: 0,
                    depth: 250,
                    modifier: .5,
                    slideShadows: false,
                },
                breakpoints: {
                    1199: {
                        slidesPerView: 3,
                        spaceBetween: 30,
                    },
                    991: {
                        slidesPerView: 3,
                        spaceBetween: 10,
                    },
                    767: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    575: {
                        slidesPerView: 1,
                        spaceBetween: 5,
                    }
                }
            });

        }
    }
        $('.collapse').on('shown.bs.collapse', function () {
            $(this).prev().addClass('active');
        });

        $('.collapse').on('hidden.bs.collapse', function () {
            $(this).prev().removeClass('active');
        });

    $('.popup-video').magnificPopup({
        disableOn: 700,
        mainClass: 'mfp-fade',
        removalDelay: 0,
        preloader: false,
        fixedContentPos: false,
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">' +
                '<div class="mfp-close"></div>' +
                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                '</div>',
            patterns: {
                youtube: {
                    index: 'youtube.com/',
                    id: 'v=',
                    src: '//www.youtube.com/embed/%id%?autoplay=1'
                }
            },
            srcAction: 'iframe_src',
        }
    });




new WOW().init();
});



    $(window).ready(function() {
        $('#preload').delay(200).fadeOut('fade');
    });
    function appScreenshotCarousel () {
    if ($('.app-section-slider').length) {

        var swiper = new Swiper('.app-section-slider', {
            effect: 'coverflow',
            loop: true,
            centeredSlides: true,
            slidesPerView: 4,
            initialSlide: 4,
            keyboardControl: true,
            mousewheelControl: false,
            lazyLoading: true,
            preventClicks: false,
            preventClicksPropagation: false,
            lazyLoadingInPrevNext: true,
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            coverflow: {
                rotate: 0,
                stretch: 0,
                depth: 250,
                modifier: .5,
                slideShadows: false,
            },
            breakpoints: {
                1199: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
                991: {
                    slidesPerView: 3,
                    spaceBetween: 10,
                },
                767: {
                    slidesPerView: 2,
                    spaceBetween: 10,
                },
                575: {
                    slidesPerView: 1,
                    spaceBetween: 5,
                }
            }
        });

    }
}

// instance of fuction while Window Load event
jQuery(window).on('load', function() {
    (function($) {
       appScreenshotCarousel();

    })(jQuery);
});

/*========Stats Counter Setup========*/
    (function(){
        if($(".counter-area").length > 0) {
            var a = 0;
            $(window).on('scroll', function() {
                var oTop = $('.counter-area').offset().top - window.innerHeight;
                if (a == 0 && $(window).scrollTop() > oTop) {
                    $('.counter-area .counter-item .counter').each(function() {
                        var $this = $(this),
                            countTo = $this.attr('data-count');
                        $({
                            countNum: $this.text()
                        }).animate({
                            countNum: countTo
                        }, {
                            duration: 2000,
                            easing: 'swing',
                            step: function() {
                                $this.text(Math.floor(this.countNum));
                            },
                            complete: function() {
                                $this.text(this.countNum);
                            }
                        });
                    });
                    a = 1;
                }
            });
        }

    })();
        $(window).ready(function() {
        $('#preload').delay(200).fadeOut('fade');
    });
