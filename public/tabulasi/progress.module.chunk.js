webpackJsonp(["progress.module"],{

/***/ "../../../../../src/app/progress/progress.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"pcoded-wrapper\">\r\n    <nav id=\"main_navbar\" class=\" pcoded-navbar\" navbar-theme=\"theme\" active-item-theme=\"theme\" sub-item-theme=\"theme\" active-item-style=\"style6\" pcoded-navbar-position=\"fixed\" (clickOutside)=\"onClickedOutside($event)\" [exclude]=\"'#mobile-collapse'\">\r\n      <div class=\"sidebar_toggle\"><a href=\"javascript:;\"><i class=\"icon-close icons\"></i></a></div>\r\n      <div class=\"pcoded-inner-navbar main-menu\" appAccordion slimScroll width=\"100%\" height=\"100%\" size=\"4px\" color=\"#fff\" opacity=\"0.3\" allowPageScroll=\"false\">\r\n        <div class=\"\">\r\n          <div class=\"main-menu-header\">\r\n            <div class=\"user-details\">\r\n              <span><i class=\"icofont icofont-ui-calendar\" style=\"padding-right: 5px; font-size: 15px;\"></i> {{ now | date:'dd MMMM yyyy '}}</span>\r\n              <span><i class=\"icofont icofont-ui-clock\" style=\"padding-right: 5px; font-size: 15px;\"></i> {{ now | date:'HH:mm:ss'}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div>\r\n          <form class=\"md-float-material\" [formGroup]=\"myForm\" (ngSubmit)=\"onSubmit()\">\r\n          <!-- <form class=\"md-float-material\" (ngSubmit)=\"onSubmit()\"> -->\r\n          <div class=\"card table-card\">\r\n            <div class=\"col-sm-12 card-block\" style=\"padding-top: 10px;padding-bottom: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 5px;\">\r\n                    <h4 class=\"sub-title\">Status Validasi</h4>\r\n                </div> \r\n                <div class=\"col-sm-12\">\r\n                  <div class=\"form-radio\" id=\"radio-group\"> \r\n                      <div class=\"radio radio-inline\">\r\n                        <label>\r\n                          <input type=\"radio\" name=\"validasi\" formControlName=\"validasi\" value=\"true\">\r\n                          <i class=\"helper\"></i>Sudah Validasi\r\n                        </label>\r\n                      </div>\r\n                      <div class=\"radio radio-inline\">\r\n                        <label>\r\n                          <input type=\"radio\" name=\"validasi\" formControlName=\"validasi\" value=\"false\" checked=\"checked\">\r\n                          <i class=\"helper\"></i>Belum Validasi\r\n                        </label>\r\n                      </div> \r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"card table-card\">\r\n            <div class=\"col-sm-12 card-block\" style=\"padding-top: 10px;padding-bottom: 10px;\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Provinsi</h4>\r\n                  <select id=\"provinsi\" class=\"form-control\" formControlName=\"provinsi\" (change)=\"onProvinsiChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let prov of provinsi | async\" value=\"{{prov.kode_provinsi}}\">{{prov.nama_provinsi}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Kota</h4>\r\n                  <select id=\"kota\" class=\"form-control\" formControlName=\"kota\" (change)=\"onKotaChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let kot of kota | async\" value=\"{{kot.kode_kota}}\">{{kot.nama_kota}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Kecamatan</h4>\r\n                  <select id=\"kecamatan\" class=\"form-control\" formControlName=\"kecamatan\" (change)=\"onKecamatanChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let kec of kecamatan | async\" value=\"{{kec.kode_kecamatan}}\">{{kec.nama_kecamatan}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">Kelurahan</h4>\r\n                  <select id=\"kelurahan\" class=\"form-control\" formControlName=\"kelurahan\" (change)=\"onKelurahanChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let kel of kelurahan | async\" value=\"{{kel.kode_kelurahan}}\">{{kel.nama_kelurahan}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">\r\n                  <h4 class=\"sub-title\">TPS</h4>\r\n                  <select id=\"tps\" class=\"form-control\" formControlName=\"tps\" (change)=\"onTPSChange($event)\"> \r\n                    <option value=\"\">-- Pilih --</option>\r\n                    <option *ngFor=\"let tp of tps | async\" value=\"{{tp.kode_tps}}\">{{tp.nama_tps}}</option>\r\n                  </select>\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 10px;\">  \r\n                    <button type=\"submit\" class=\"btn btn-info btn-sm txt-white p-t-10 p-b-10\" style=\"width:100%\"><i class=\"icofont icofont-ui-search\"></i>Tampilkan</button>  \r\n  \r\n                    <!-- <button type=\"button\" class=\"btn btn-info btn-sm txt-white p-t-10 p-b-10\" style=\"width:100%\" (click)=\"changeData()\"><i class=\"icofont icofont-ui-search\"></i>Change</button>  \r\n                    <button type=\"button\" class=\"btn btn-info btn-sm txt-white p-t-10 p-b-10\" style=\"width:100%\" (click)=\"readData()\"><i class=\"icofont icofont-ui-search\"></i>Read</button>   -->\r\n                </div>\r\n                <div class=\"col-sm-12\" style=\"margin-bottom: 20px;\"> \r\n                  <br>\r\n                  <br>\r\n                  <br>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    </nav>\r\n    <div class=\"pcoded-content\">\r\n      <div class=\"pcoded-inner-content\">\r\n        <div class=\"main-body\">\r\n          <div class=\"page-wrapper\">\r\n            <!-- <app-title></app-title> -->\r\n            <!-- <app-breadcrumbs></app-breadcrumbs> -->\r\n            <div class=\"page-body\">\r\n              <!-- <router-outlet><app-spinner> -->\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-12 col-sm-12\">\r\n                    <app-card style=\"text-align: center;\">\r\n                      <h5>{{_header_title}}</h5>\r\n                    </app-card>\r\n                    <br/>\r\n                    <br/>\r\n                    <app-card [title]=\"'GRAFIK HASIL PEROLEHAN SUARA'\" [headerContent]=\"''\">\r\n                      <div class=\"row\">\r\n                        <div class=\"col-md-6 col-sm-12\">\r\n                          <nvd3 [options]=\"discreteBarOptions\" [data]=\"discreteBarData\"></nvd3>\r\n                        </div>\r\n                        <div class=\"col-md-6 col-sm-12\">\r\n                          <br/><br/><br/> \r\n                          <div>\r\n                            <label class=\"badge badge-lg bg-danger\" style=\"vertical-align: bottom;\"><i class=\"icofont icofont-ui-press\"></i></label>  \r\n                            <h5 *ngIf=\"_dataLoaded\">\r\n                              &nbsp;&nbsp;Joko Widodo - KH Ma'ruf Amin <br>\r\n                              <b>&nbsp;&nbsp;{{numberPercen(_dataPersen1)}}% ({{numberWithCommas(_data1)}} suara) </b>\r\n                            </h5>\r\n                          </div>\r\n                          <br>\r\n                          \r\n                          <div>\r\n                            <label class=\"badge badge-lg bg-success\" style=\"vertical-align: bottom;\"><i class=\"icofont icofont-ui-press\"></i></label>  \r\n                            <h5 *ngIf=\"_dataLoaded\">\r\n                              &nbsp;&nbsp;Prabowo Subianto - Sandiaga Uno <br>\r\n                              <b>&nbsp;&nbsp;{{numberPercen(_dataPersen2)}}% ({{numberWithCommas(_data2)}} suara) </b>\r\n                            </h5>\r\n                          </div>\r\n                        </div> \r\n                      </div> \r\n                    </app-card>\r\n                  </div>\r\n                  \r\n                  <div class=\"col-md-12 col-sm-12\">\r\n                      <br>\r\n                      <br>\r\n                  </div>\r\n                  \r\n                  <div class=\"col-md-12 col-sm-12\">\r\n                    <app-card [title]=\"'PROGRESS SUARA MASUK'\" [headerContent]=\"''\">\r\n                      <div style=\"text-align: center\" *ngIf=\"_dataLoaded_detail\" > \r\n                        <div class=\"p-10 card user-card\" style=\"margin-bottom: 10px;\" *ngFor=\"let data of _dataTable\">\r\n                          <h5 style=\"margin-bottom: 5px\">{{data.nama_wilayah}} ({{data.tps_masuk}}/{{data.tps_total}} TPS)</h5>\r\n                          <div class=\"progress md-progress\" style=\"height: 30px\">\r\n                              <div class=\"progress-bar\" role=\"progressbar\" style=\"height: 30px\" [style.width]=\"data.tps_persen + '%'\" aria-valuenow=\"\" aria-valuemin=\"0\" aria-valuemax=\"\">{{numberPercen(data.tps_persen)}}%</div>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </app-card>\r\n                  </div>\r\n                </div>\r\n              <!-- </app-spinner></router-outlet> -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div> "

/***/ }),

/***/ "../../../../../src/app/progress/progress.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layouts_admin_admin_layout_component__ = __webpack_require__("../../../../../src/app/layouts/admin/admin-layout.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__ = __webpack_require__("../../../../../src/app/shared/elements/animation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__ = __webpack_require__("../../../../angularfire2/firestore/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_d3__ = __webpack_require__("../../../../d3/d3.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_d3___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_d3__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProgressComponent = (function () {
    function ProgressComponent(adminLayout, db) {
        var _this = this;
        this.adminLayout = adminLayout;
        this.db = db;
        this.deviceType = 'desktop';
        this.verticalNavType = 'expanded';
        this.verticalEffect = 'shrink';
        this.isCollapsedMobile = 'no-block';
        this.isCollapsedSideBar = 'no-block';
        this.toggleOn = true;
        adminLayout.setRoute();
        setInterval(function () {
            _this.now = Date.now();
        }, 1);
        this.myForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            validasi: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](),
            provinsi: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](),
            kota: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](),
            kecamatan: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](),
            kelurahan: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](),
            tps: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](),
        });
        this.myForm.get('validasi').setValue('false');
        this.myForm.get('provinsi').setValue('');
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        this.getProvinsi();
        this._header_title = 'Nasional';
    }
    ProgressComponent.prototype.ngOnInit = function () {
        // load data
        this.getData('test_nasional', '0', 'validasi_non');
        this.getDataDetail('test_provinsi', '', 'validasi_non');
    };
    ProgressComponent.prototype.onClickedOutside = function (e) {
        if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
            this.toggleOn = true;
            this.verticalNavType = 'offcanvas';
        }
    };
    ProgressComponent.prototype.getProvinsi = function () {
        this.provinsi = this.db.collection('/test_provinsi', function (ref) { return ref.orderBy('nama_provinsi', 'asc'); }).valueChanges();
    };
    ProgressComponent.prototype.getKota = function (kode_provinsi) {
        this.kota = this.db.collection('/test_kota', function (ref) { return ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc'); }).valueChanges();
    };
    ProgressComponent.prototype.getKecamatan = function (kode_kota) {
        this.kecamatan = this.db.collection('/test_kecamatan', function (ref) { return ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc'); }).valueChanges();
    };
    ProgressComponent.prototype.getKelurahan = function (kode_kecamatan) {
        this.kelurahan = this.db.collection('/test_kelurahan', function (ref) { return ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc'); }).valueChanges();
    };
    ProgressComponent.prototype.getTPS = function (kode_kelurahan) {
        this.tps = this.db.collection('/test_tps', function (ref) { return ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc'); }).valueChanges();
    };
    ProgressComponent.prototype.onProvinsiChange = function (event) {
        this.kota = null;
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._provinsi = selectElementText;
        }
        else {
            this._provinsi = null;
        }
        this._kota = null;
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;
        this.getKota(event.target.value);
    };
    ProgressComponent.prototype.onKotaChange = function (event) {
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kota = selectElementText;
        }
        else {
            this._kota = null;
        }
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;
        this.getKecamatan(event.target.value);
    };
    ProgressComponent.prototype.onKecamatanChange = function (event) {
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kecamatan = selectElementText;
        }
        else {
            this._kecamatan = null;
        }
        this._kelurahan = null;
        this._tps = null;
        this.getKelurahan(event.target.value);
    };
    ProgressComponent.prototype.onKelurahanChange = function (event) {
        this.tps = null;
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kelurahan = selectElementText;
        }
        else {
            this._kelurahan = null;
        }
        this._tps = null;
        this.getTPS(event.target.value);
    };
    ProgressComponent.prototype.onTPSChange = function (event) {
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._tps = selectElementText;
        }
        else {
            this._tps = null;
        }
    };
    ProgressComponent.prototype.onSubmit = function () {
        var form_value = this.myForm.value;
        var form_json = JSON.stringify(form_value);
        console.log(form_json);
        var validasi = 'validasi_non';
        if (form_value['validasi'] === 'true') {
            validasi = 'validasi';
        }
        else {
            validasi = 'validasi_non';
        }
        if (form_value['tps'] !== '') {
            this.getDataTPS('test_tps', form_value['tps'], validasi);
            this.getDataTPSSatuan('test_tps', form_value['tps'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan + ' - ' + this._tps;
        }
        else if (form_value['kelurahan'] !== '') {
            this.getData('test_kelurahan', form_value['kelurahan'], validasi);
            this.getDataTPSDetail('test_tps', form_value['kelurahan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan;
        }
        else if (form_value['kecamatan'] !== '') {
            this.getData('test_kecamatan', form_value['kecamatan'], validasi);
            this.getDataDetail('test_kelurahan', form_value['kecamatan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan;
        }
        else if (form_value['kota'] !== '') {
            this.getData('test_kota', form_value['kota'], validasi);
            this.getDataDetail('test_kecamatan', form_value['kota'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota;
        }
        else if (form_value['provinsi'] !== '') {
            this.getData('test_provinsi', form_value['provinsi'], validasi);
            this.getDataDetail('test_kota', form_value['provinsi'], validasi);
            this._header_title = 'PROV ' + this._provinsi;
        }
        else {
            this.getData('test_nasional', '0', validasi);
            this.getDataDetail('test_provinsi', '0', validasi);
            this._header_title = 'Nasional';
        }
    };
    ProgressComponent.prototype.getData = function (level, kode, validasi) {
        var _this = this;
        //  get Data
        this.db.collection('/' + level).doc(kode)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            _this._kode_wilayah = '';
            _this._nama_wilayah = '';
            if (level === 'test_provinsi') {
                _this._kode_wilayah = item['kode_provinsi'];
                _this._nama_wilayah = item['nama_provinsi'];
            }
            else if (level === 'test_kota') {
                _this._kode_wilayah = item['kode_provinsi'];
                _this._nama_wilayah = item['nama_provinsi'];
            }
            else if (level === 'test_kecamatan') {
                _this._kode_wilayah = item['kode_kecamatan'];
                _this._nama_wilayah = item['nama_kecamatan'];
            }
            else if (level === 'test_kelurahan') {
                _this._kode_wilayah = item['kode_kelurahan'];
                _this._nama_wilayah = item['nama_kelurahan'];
            }
            else if (level === 'test_tps') {
                _this._kode_wilayah = item['kode_tps'];
                _this._nama_wilayah = item['nama_tps'];
            }
            _this._wilayah_latitude = item['nasional_latitude'];
            _this._wilayah_longitude = item['nasional_longitude'];
            _this._validasi = item['validasi'];
            _this._validasi_non = item['validasi_non'];
            // change chart
            _this._data1 = item[validasi]['suara_1'];
            _this._data2 = item[validasi]['suara_2'];
            _this._tpsMasuk = item[validasi]['tps_masuk'];
            _this._tpsBelum = item[validasi]['tps_belum'];
            _this._tpsTotal = item[validasi]['tps_total'];
            _this._dataPersen1 = item[validasi]['suara_1'] / item[validasi]['suara_total'] * 100;
            _this._dataPersen2 = item[validasi]['suara_2'] / item[validasi]['suara_total'] * 100;
            _this._dataLoaded = true;
            _this.changeChart(_this._data1, _this._data2, _this._dataPersen1, _this._dataPersen2);
        });
    };
    ProgressComponent.prototype.getDataDetail = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        // if (kode === '') {
        //  get Data
        this.db.collection('/' + level)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            item.forEach(function (it) {
                var temp = {};
                temp['kode_wilayah'] = '';
                temp['nama_wilayah'] = '';
                if (level === 'test_provinsi') {
                    temp['kode_wilayah'] = it['kode_provinsi'];
                    temp['nama_wilayah'] = it['nama_provinsi'];
                }
                else if (level === 'test_kota') {
                    temp['kode_wilayah'] = it['kode_kota'];
                    temp['nama_wilayah'] = it['nama_kota'];
                }
                else if (level === 'test_kecamatan') {
                    temp['kode_wilayah'] = it['kode_kecamatan'];
                    temp['nama_wilayah'] = it['nama_kecamatan'];
                }
                else if (level === 'test_kelurahan') {
                    temp['kode_wilayah'] = it['kode_kelurahan'];
                    temp['nama_wilayah'] = it['nama_kelurahan'];
                }
                else if (level === 'test_tps') {
                    temp['kode_wilayah'] = it['kode_tps'];
                    temp['nama_wilayah'] = it['nama_tps'];
                }
                temp['suara_1'] = it[validasi]['suara_1'];
                temp['suara_2'] = it[validasi]['suara_2'];
                temp['tps_masuk'] = it[validasi]['tps_masuk'];
                temp['tps_belum'] = it[validasi]['tps_belum'];
                temp['tps_total'] = it[validasi]['tps_total'];
                temp['tps_persen'] = (Number(it[validasi]['tps_masuk']) / Number(it[validasi]['tps_total'])) * 100;
                temp['suara_total'] = it[validasi]['suara_1'] + it[validasi]['suara_2'];
                total1 = total1 + temp['suara_1'];
                total2 = total2 + temp['suara_2'];
                total = total + total1 + total2;
                table_json.push(temp);
            });
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
        // }
    };
    ProgressComponent.prototype.getDataTPS = function (level, kode, validasi) {
        var _this = this;
        //  get Data
        this.db.collection('/' + level).doc(kode)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            _this._kode_wilayah = item['kode_tps'];
            _this._nama_wilayah = item['nama_tps'];
            _this._wilayah_latitude = item['nasional_latitude'];
            _this._wilayah_longitude = item['nasional_longitude'];
            // change chart
            _this._data1 = Number(item['suara_1']);
            _this._data2 = Number(item['suara_2']);
            _this._dataPersen1 = Number(item['suara_1']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
            _this._dataPersen2 = Number(item['suara_2']) / (Number(item['suara_1']) + Number(item['suara_2'])) * 100;
            _this._dataLoaded = true;
            _this.changeChart(_this._data1, _this._data2, _this._dataPersen1, _this._dataPersen2);
        });
    };
    ProgressComponent.prototype.getDataTPSDetail = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        //  get Data
        this.db.collection('/' + level)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            item.forEach(function (it) {
                var temp = {};
                temp['kode_wilayah'] = it['kode_tps'];
                temp['nama_wilayah'] = it['nama_tps'];
                temp['suara_1'] = it['suara_1'];
                temp['suara_2'] = it['suara_2'];
                if (temp['suara_1'] !== 0 && temp['suara_2'] !== 0) {
                    temp['tps_masuk'] = 1;
                }
                else {
                    temp['tps_masuk'] = 0;
                }
                temp['tps_total'] = 1;
                temp['tps_persen'] = (Number(temp['tps_masuk']) / Number(temp['tps_total'])) * 100;
                temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
                total1 = total1 + Number(temp['suara_1']);
                total2 = total2 + Number(temp['suara_2']);
                total = Number(total1) + Number(total2);
                table_json.push(temp);
            });
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
    };
    ProgressComponent.prototype.getDataTPSSatuan = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        //  get Data
        this.db.collection('/' + level).doc(kode)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            var it = item;
            var temp = {};
            temp['kode_wilayah'] = it['kode_tps'];
            temp['nama_wilayah'] = it['nama_tps'];
            temp['suara_1'] = it['suara_1'];
            temp['suara_2'] = it['suara_2'];
            temp['tps_masuk'] = it['tps_masuk'];
            temp['tps_belum'] = it['tps_belum'];
            temp['tps_total'] = it['tps_total'];
            if (temp['suara_1'] !== 0 && temp['suara_2'] !== 0) {
                temp['tps_masuk'] = 1;
            }
            else {
                temp['tps_masuk'] = 0;
            }
            temp['tps_total'] = 1;
            temp['tps_persen'] = (Number(temp['tps_masuk']) / Number(temp['tps_total'])) * 100;
            temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
            total1 = temp['suara_1'];
            total2 = temp['suara_2'];
            total = temp['suara_total'];
            table_json.push(temp);
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
    };
    ProgressComponent.prototype.changeChart = function (data1, data2, dataPersen1, dataPersen2) {
        this.discreteBarOptions = {
            chart: {
                type: 'discreteBarChart',
                height: 500,
                // width: 600,
                margin: {
                    top: 50,
                    right: 50,
                    bottom: 50,
                    left: 50
                },
                x: function (d) {
                    return d.label;
                },
                y: function (d) {
                    return d.value;
                },
                xAxis: {
                    showMaxMin: false,
                    tickPadding: 15
                },
                yAxis: {
                    tickFormat: function (d) {
                        return d3.format(',.0f')(d);
                    },
                    tickPadding: 10
                },
                valueFormat: d3.format(',.0f'),
                staggerLabels: false,
                showValues: true,
                tooltips: true
            }
        };
        this.discreteBarData = [{
                key: 'Cumulative Return',
                values: [{
                        'label': 'Joko Widodo - KH Ma\'ruf Amin',
                        'value': data1,
                        'color': '#DA201B'
                    }, {
                        'label': 'Prabowo Subianto - Sandiaga Uno',
                        'value': data2,
                        'color': '#00A14D'
                    }]
            }];
    };
    ProgressComponent.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    };
    ProgressComponent.prototype.numberPercen = function (x) {
        return Number(x).toFixed(2);
    };
    return ProgressComponent;
}());
ProgressComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-progress',
        template: __webpack_require__("../../../../../src/app/progress/progress.component.html"),
        styles: [__webpack_require__("../../../../c3/c3.min.css"), __webpack_require__("../../../../../src/assets/icon/SVG-animated/svg-weather.css"), __webpack_require__("../../../../../src/assets/css/chartist.css"), __webpack_require__("../../../../nvd3/build/nv.d3.css")],
        encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
        animations: [__WEBPACK_IMPORTED_MODULE_2__shared_elements_animation__["a" /* fadeInOutTranslate */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__["a" /* AngularFirestore */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_angularfire2_firestore__["a" /* AngularFirestore */]) === "function" && _b || Object])
], ProgressComponent);

var _a, _b;
//# sourceMappingURL=progress.component.js.map

/***/ }),

/***/ "../../../../../src/app/progress/progress.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgressModule", function() { return ProgressModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__progress_component__ = __webpack_require__("../../../../../src/app/progress/progress.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__progress_routing__ = __webpack_require__("../../../../../src/app/progress/progress.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ProgressModule = (function () {
    function ProgressModule() {
    }
    return ProgressModule;
}());
ProgressModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__progress_routing__["a" /* ProgressRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__progress_component__["a" /* ProgressComponent */]]
    })
], ProgressModule);

//# sourceMappingURL=progress.module.js.map

/***/ }),

/***/ "../../../../../src/app/progress/progress.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__progress_component__ = __webpack_require__("../../../../../src/app/progress/progress.component.ts");

var ProgressRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__progress_component__["a" /* ProgressComponent */],
        data: {
            breadcrumb: 'Progress',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }];
//# sourceMappingURL=progress.routing.js.map

/***/ })

});
//# sourceMappingURL=progress.module.chunk.js.map