webpackJsonp(["tabulasi.module"],{

/***/ "../../../../../src/app/tabulasi/tabulasi.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabulasiModule", function() { return TabulasiModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabulasi_component__ = __webpack_require__("../../../../../src/app/tabulasi/tabulasi.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabulasi_routing__ = __webpack_require__("../../../../../src/app/tabulasi/tabulasi.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var TabulasiModule = (function () {
    function TabulasiModule() {
    }
    return TabulasiModule;
}());
TabulasiModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(__WEBPACK_IMPORTED_MODULE_4__tabulasi_routing__["a" /* TabulasiRoutes */]),
            __WEBPACK_IMPORTED_MODULE_5__shared_shared_module__["a" /* SharedModule */]
        ],
        declarations: [__WEBPACK_IMPORTED_MODULE_3__tabulasi_component__["a" /* TabulasiComponent */]]
    })
], TabulasiModule);

//# sourceMappingURL=tabulasi.module.js.map

/***/ }),

/***/ "../../../../../src/app/tabulasi/tabulasi.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabulasiRoutes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tabulasi_component__ = __webpack_require__("../../../../../src/app/tabulasi/tabulasi.component.ts");

var TabulasiRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__tabulasi_component__["a" /* TabulasiComponent */],
        data: {
            breadcrumb: 'Tabulasi',
            icon: 'icofont-home bg-c-blue',
            status: false
        }
    }];
//# sourceMappingURL=tabulasi.routing.js.map

/***/ })

});
//# sourceMappingURL=tabulasi.module.chunk.js.map