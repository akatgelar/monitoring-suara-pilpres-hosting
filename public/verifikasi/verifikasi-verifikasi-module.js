(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["verifikasi-verifikasi-module"],{

/***/ "./src/app/layout/verifikasi/verifikasi-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/verifikasi/verifikasi-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: VerifikasiRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifikasiRoutingModule", function() { return VerifikasiRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _verifikasi_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./verifikasi.component */ "./src/app/layout/verifikasi/verifikasi.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _verifikasi_component__WEBPACK_IMPORTED_MODULE_2__["VerifikasiComponent"]
    }
];
var VerifikasiRoutingModule = /** @class */ (function () {
    function VerifikasiRoutingModule() {
    }
    VerifikasiRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], VerifikasiRoutingModule);
    return VerifikasiRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/verifikasi/verifikasi.component.html":
/*!*************************************************************!*\
  !*** ./src/app/layout/verifikasi/verifikasi.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"card table-card\">\r\n  <form class=\"md-float-material\" [formGroup]=\"myForm\" (ngSubmit)=\"onSubmit()\">\r\n  <div class=\"col-sm-12 card-block\" style=\"padding-top: 10px;padding-bottom: 10px;\">\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-2\">\r\n        <div class=\"form-radio\" id=\"radio-group\" style=\"padding-top: 20px;\">  \r\n          <div class=\"radio radio-inline\">\r\n            <label>\r\n              <input type=\"radio\" name=\"validasi\" formControlName=\"validasi\" value=\"true\">\r\n              <i class=\"helper\"></i>Sudah Validasi\r\n            </label>\r\n          </div>\r\n          <div class=\"radio radio-inline\">\r\n            <label>\r\n              <input type=\"radio\" name=\"validasi\" formControlName=\"validasi\" value=\"false\" checked=\"checked\">\r\n              <i class=\"helper\"></i>Belum Validasi\r\n            </label>\r\n          </div> \r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-2\" style=\"margin-bottom: 10px;\">\r\n        <h5 class=\"sub-title\">Provinsi</h5>\r\n        <select id=\"provinsi\" class=\"form-control\" formControlName=\"provinsi\" (change)=\"onProvinsiChange($event)\"> \r\n          <option value=\"\">-- Pilih --</option>\r\n          <option *ngFor=\"let prov of provinsi | async\" value=\"{{prov.kode_provinsi}}\">{{prov.nama_provinsi}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"col-sm-2\" style=\"margin-bottom: 10px;\">\r\n        <h5 class=\"sub-title\">Kota</h5>\r\n        <select id=\"kota\" class=\"form-control\" formControlName=\"kota\" (change)=\"onKotaChange($event)\"> \r\n          <option value=\"\">-- Pilih --</option>\r\n          <option *ngFor=\"let kot of kota | async\" value=\"{{kot.kode_kota}}\">{{kot.nama_kota}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"col-sm-2\" style=\"margin-bottom: 10px;\">\r\n        <h5 class=\"sub-title\">Kecamatan</h5>\r\n        <select id=\"kecamatan\" class=\"form-control\" formControlName=\"kecamatan\" (change)=\"onKecamatanChange($event)\"> \r\n          <option value=\"\">-- Pilih --</option>\r\n          <option *ngFor=\"let kec of kecamatan | async\" value=\"{{kec.kode_kecamatan}}\">{{kec.nama_kecamatan}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"col-sm-2\" style=\"margin-bottom: 10px;\">\r\n        <h5 class=\"sub-title\">Kelurahan</h5>\r\n        <select id=\"kelurahan\" class=\"form-control\" formControlName=\"kelurahan\" (change)=\"onKelurahanChange($event)\"> \r\n          <option value=\"\">-- Pilih --</option>\r\n          <option *ngFor=\"let kel of kelurahan | async\" value=\"{{kel.kode_kelurahan}}\">{{kel.nama_kelurahan}}</option>\r\n        </select>\r\n      </div>\r\n      <div class=\"col-sm-2\" style=\"margin-bottom: 10px;\">  \r\n        <h5 class=\"sub-title\">&nbsp;</h5>\r\n        <button type=\"submit\" class=\"btn btn-info btn-md txt-white p-t-10 p-b-10\" style=\"width:100%\"><i class=\"fa fa-search\"></i> Tampilkan</button>  \r\n      </div>\r\n    </div>\r\n  </div>\r\n  </form>\r\n</div>\r\n<br/>\r\n<!-- <button type=\"button\" class=\"btn btn-primary btn-md txt-white p-t-10 p-b-10\" (click)=\"open(content, '')\"><i class=\"fa fa-search\"></i> Detail</button> -->\r\n\r\n\r\n\r\n<div class=\"card table-card\">\r\n  <table class=\"table table table-bordered\">\r\n    <thead style=\"background-color: #F5F5F5;\">\r\n    <tr>\r\n      <th rowspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Wilayah</th>\r\n      <th colspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Perolehan Suara</th>\r\n      <th rowspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Total Suara</th> \r\n      <th rowspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Status Verifikasi</th> \r\n      <th rowspan=\"2\" style=\"text-align: center;vertical-align: middle;\">Aksi</th> \r\n    </tr> \r\n    <tr>\r\n      <th style=\"text-align: center;vertical-align: middle;\">Joko Widodo -<br> KH Ma'ruf Amin</th>\r\n      <th style=\"text-align: center;vertical-align: middle;\">Prabowo Subianto -<br> Sandiaga Uno</th> \r\n    </tr>\r\n    </thead>\r\n    <tbody style=\"text-align: center\" *ngIf=\"_dataLoaded_detail\" > \r\n    <tr *ngFor=\"let data of _dataTable\">\r\n      <th scope=\"row\">{{data.nama_wilayah}}</th>\r\n      <td>{{data.suara_1}}</td>\r\n      <td>{{data.suara_2}}</td>\r\n      <td>{{data.suara_total}}</td>\r\n      <td>\r\n          <span *ngIf=\"data.status_validasi === true\" class=\"badge badge-success\"><i class=\"fa fa-check\"></i></span>\r\n          <span *ngIf=\"data.status_validasi === false\" class=\"badge badge-danger\"><i class=\"fa fa-close\"></i></span>\r\n      </td>\r\n      <td>\r\n          <button type=\"button\" class=\"btn btn-primary btn-md txt-white p-t-10 p-b-10\" (click)=\"open(content, data)\"><i class=\"fa fa-search\"></i> Detail</button>\r\n      </td>\r\n    </tr>\r\n    </tbody>\r\n    <tfoot style=\"text-align: center; background-color: #F5F5F5;\" *ngIf=\"_dataLoaded_detail\">\r\n        <tr>\r\n          <th style=\"text-align: left;vertical-align: middle;\">Total</th>\r\n          <th style=\"text-align: center;vertical-align: middle;\">{{_dataTableTotal1}}</th>\r\n          <th style=\"text-align: center;vertical-align: middle;\">{{_dataTableTotal2}}</th>\r\n          <th style=\"text-align: center;vertical-align: middle;\">{{_dataTableTotal}}</th>\r\n          <th style=\"text-align: left;vertical-align: middle;\"></th>\r\n          <th style=\"text-align: left;vertical-align: middle;\"></th>\r\n        </tr>\r\n    </tfoot>\r\n  </table>\r\n</div>\r\n\r\n<ng-template #content let-c=\"close\" let-d=\"dismiss\" size=\"lg\">\r\n    <form class=\"md-float-material\" [formGroup]=\"myForm2\" (ngSubmit)=\"onSubmit2()\">\r\n    <div class=\"modal-header\">\r\n        <h4 class=\"modal-title\">Verifikasi TPS </h4> &nbsp;&nbsp;&nbsp;&nbsp;\r\n        <input type=\"text\" formControlName=\"kode_tps\"  style=\"width: 300px;height: 30px;text-align: left;font-size: 30px;\"/>\r\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n            <span aria-hidden=\"true\">&times;</span>\r\n        </button>\r\n    </div>\r\n    <div class=\"modal-body\">\r\n      <div class=\"row\">\r\n          <div class=\"col-sm-3\" style=\"margin-bottom: 10px; text-align: center;\">\r\n            <h5 class=\"sub-title\">Joko Widodo - KH Ma'ruf Amin</h5>\r\n            <input type=\"text\" formControlName=\"suara_1\"   style=\"width: 200px;height: 70px;text-align: center;font-size: 40px;\"/>\r\n          </div>\r\n          <div class=\"col-sm-3\" style=\"margin-bottom: 10px; text-align: center;\">\r\n            <h5 class=\"sub-title\">Prabowo Subianto - Sandiaga Uno</h5>\r\n            <input type=\"text\" formControlName=\"suara_2\"   style=\"width: 200px;height: 70px;text-align: center;font-size: 40px;\"/>\r\n          </div> \r\n          <div class=\"col-sm-3\" style=\"margin-bottom: 10px; text-align: center;\">\r\n            <table>\r\n              <tr>\r\n                <td>Suara Sah</td>\r\n                <td><input type=\"text\" formControlName=\"suara_sah\"  style=\"width: 200px;height: 70px;text-align: center;font-size: 40px;\"/></td>\r\n              </tr>\r\n              <tr>\r\n                <td>Suara Tidak Sah</td>\r\n                <td><input type=\"text\" formControlName=\"suara_tidaksah\"   style=\"width: 200px;height: 70px;text-align: center;font-size: 40px;\"/></td>\r\n              </tr>\r\n              <tr>\r\n                <td>Suara Total</td>\r\n                <td><input type=\"text\" formControlName=\"suara_total\"   style=\"width: 200px;height: 70px;text-align: center;font-size: 40px;\"/></td>\r\n              </tr>\r\n            </table>\r\n          </div>\r\n          <div class=\"col-sm-3\" style=\"margin-bottom: 10px; text-align: center;\">\r\n              <h5 class=\"sub-title\">&nbsp;</h5>\r\n              <button type=\"submit\"  class=\"btn btn-success btn-md txt-white p-t-10 p-b-10\" style=\"width: 150px;height: 70px;text-align: center;font-size: 20px;\"><i class=\"fa fa-check\"></i> OK</button>\r\n              &nbsp;&nbsp;&nbsp;&nbsp;\r\n              <button type=\"button\" (click)=\"d('Cross click')\" class=\"btn btn-danger btn-md txt-white p-t-10 p-b-10\" style=\"width: 150px;height: 70px;text-align: center;font-size: 20px;\"><i class=\"fa fa-close\"></i> TIDAK</button>\r\n          </div>\r\n      </div>\r\n      <br/>\r\n      <br/>\r\n      <div class=\"row\">\r\n        <div class=\"col-sm-4\" style=\"margin-bottom: 10px;\">\r\n          <img [src]=\"pub_suara_scan_1\" style=\"max-height:450px; width: 100%; object-fit: contain;\">\r\n        </div>\r\n        <div class=\"col-sm-4\" style=\"margin-bottom: 10px;\">\r\n          <img [src]=\"pub_suara_scan_2\" style=\"max-height:450px; width: 100%; object-fit: contain;\">\r\n        </div>\r\n        <div class=\"col-sm-4\" style=\"margin-bottom: 10px;\">\r\n          <img [src]=\"pub_suara_scan_3\" style=\"max-height:450px; width: 100%; object-fit: contain;\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </form>\r\n</ng-template>"

/***/ }),

/***/ "./src/app/layout/verifikasi/verifikasi.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/layout/verifikasi/verifikasi.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal-dialog {\n  max-width: 1000px; }\n"

/***/ }),

/***/ "./src/app/layout/verifikasi/verifikasi.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/layout/verifikasi/verifikasi.component.ts ***!
  \***********************************************************/
/*! exports provided: VerifikasiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifikasiComponent", function() { return VerifikasiComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var VerifikasiComponent = /** @class */ (function () {
    function VerifikasiComponent(db, modalService, http) {
        this.db = db;
        this.modalService = modalService;
        this.http = http;
        this.myForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            validasi: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            provinsi: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            kota: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            kecamatan: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            kelurahan: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            tps: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
        });
        this.myForm2 = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            suara_1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            suara_2: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            suara_sah: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            suara_tidaksah: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            suara_total: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            kode_tps: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
        });
        this.myForm.get('validasi').setValue('false');
        this.myForm.get('provinsi').setValue('');
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        this.getProvinsi();
        this.getDataTPSDetail('test_tps', '32.73', '');
        this.show = true;
        this.pub_suara_scan_1 = 'https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        this.pub_suara_scan_2 = 'https://www.google.co.id/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';
        this.pub_suara_scan_3 = 'https://uploads.toptal.io/blog/image/125377/toptal-blog-image-1518184225493-b9ca8a8492da6fd38f11887c066c105e.png';
    }
    VerifikasiComponent.prototype.ngOnInit = function () { };
    VerifikasiComponent.prototype.open = function (content, data) {
        var _this = this;
        if (data) {
            console.log(data);
            this.pub_suara_1 = data.suara_1;
            this.pub_suara_2 = data.suara_2;
            this.pub_suara_sah = data.suara_sah;
            this.pub_suara_tidaksah = data.suara_tidaksah;
            this.pub_suara_total = data.suara_total;
            this.pub_suara_scan_1 = data.suara_scan_1;
            this.pub_suara_scan_2 = data.suara_scan_2;
            this.pub_suara_scan_3 = data.suara_scan_3;
            this.myForm2.patchValue({
                suara_1: data.suara_1,
                suara_2: data.suara_2,
                suara_sah: data.suara_sah,
                suara_tidaksah: data.suara_tidaksah,
                suara_total: data.suara_total,
                kode_tps: data.kode_wilayah,
            });
        }
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    VerifikasiComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    VerifikasiComponent.prototype.verifikasi = function () {
        var form_value = this.myForm2.value;
        var form_json = JSON.stringify(form_value);
        console.log(form_json);
    };
    VerifikasiComponent.prototype.unverifikasi = function () {
        var form_value = this.myForm2.value;
        var form_json = JSON.stringify(form_value);
        console.log(form_json);
    };
    VerifikasiComponent.prototype.getProvinsi = function () {
        this.provinsi = this.db.collection('/test_provinsi', function (ref) { return ref.orderBy('nama_provinsi', 'asc'); }).valueChanges();
    };
    VerifikasiComponent.prototype.getKota = function (kode_provinsi) {
        this.kota = this.db.collection('/test_kota', function (ref) { return ref.where('kode_provinsi', '==', kode_provinsi).orderBy('nama_kota', 'asc'); }).valueChanges();
    };
    VerifikasiComponent.prototype.getKecamatan = function (kode_kota) {
        this.kecamatan = this.db.collection('/test_kecamatan', function (ref) { return ref.where('kode_kota', '==', kode_kota).orderBy('nama_kecamatan', 'asc'); }).valueChanges();
    };
    VerifikasiComponent.prototype.getKelurahan = function (kode_kecamatan) {
        this.kelurahan = this.db.collection('/test_kelurahan', function (ref) { return ref.where('kode_kecamatan', '==', kode_kecamatan).orderBy('nama_kelurahan', 'asc'); }).valueChanges();
    };
    VerifikasiComponent.prototype.getTPS = function (kode_kelurahan) {
        this.tps = this.db.collection('/test_tps', function (ref) { return ref.where('kode_kelurahan', '==', kode_kelurahan).orderBy('nama_tps', 'asc'); }).valueChanges();
    };
    VerifikasiComponent.prototype.onProvinsiChange = function (event) {
        this.kota = null;
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kota').setValue('');
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._provinsi = selectElementText;
        }
        else {
            this._provinsi = null;
        }
        this._kota = null;
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;
        this.getKota(event.target.value);
    };
    VerifikasiComponent.prototype.onKotaChange = function (event) {
        this.kecamatan = null;
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kecamatan').setValue('');
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kota = selectElementText;
        }
        else {
            this._kota = null;
        }
        this._kecamatan = null;
        this._kelurahan = null;
        this._tps = null;
        this.getKecamatan(event.target.value);
    };
    VerifikasiComponent.prototype.onKecamatanChange = function (event) {
        this.kelurahan = null;
        this.tps = null;
        this.myForm.get('kelurahan').setValue('');
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kecamatan = selectElementText;
        }
        else {
            this._kecamatan = null;
        }
        this._kelurahan = null;
        this._tps = null;
        this.getKelurahan(event.target.value);
    };
    VerifikasiComponent.prototype.onKelurahanChange = function (event) {
        this.tps = null;
        this.myForm.get('tps').setValue('');
        var selectElementText = event.target['options'][event.target['options'].selectedIndex].text;
        if (selectElementText !== '-- Pilih --') {
            this._kelurahan = selectElementText;
        }
        else {
            this._kelurahan = null;
        }
        this._tps = null;
        this.getTPS(event.target.value);
    };
    // onTPSChange(event) {
    //     const selectElementText = event.target['options']
    //         [event.target['options'].selectedIndex].text;
    //     if (selectElementText !== '-- Pilih --') {
    //         this._tps = selectElementText;
    //     } else {
    //         this._tps = null;
    //     }
    // }
    VerifikasiComponent.prototype.onSubmit = function () {
        var form_value = this.myForm.value;
        var form_json = JSON.stringify(form_value);
        console.log(form_json);
        var validasi = 'validasi_non';
        if (form_value['validasi'] === 'true') {
            validasi = 'validasi';
        }
        else {
            validasi = 'validasi_non';
        }
        if (form_value['kelurahan'] !== '') {
            this.getDataTPSDetail('test_tps', form_value['kelurahan'], validasi);
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan + ' - KEL ' + this._kelurahan;
        }
        else if (form_value['kecamatan'] !== '') {
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota + ' - KEC ' + this._kecamatan;
        }
        else if (form_value['kota'] !== '') {
            this._header_title = 'PROV ' + this._provinsi + ' - ' + this._kota;
        }
        else if (form_value['provinsi'] !== '') {
            this._header_title = 'PROV ' + this._provinsi;
        }
        else {
            this._header_title = 'Nasional';
        }
    };
    VerifikasiComponent.prototype.onSubmit2 = function () {
        var _this = this;
        var form_value2 = this.myForm2.value;
        var form_json2 = JSON.stringify(form_value2);
        console.log(form_json2);
        var url = '/api/verifikasiSuaraTPS';
        var data = form_json2;
        this.sendData(url, data).subscribe(function (result) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log(result);
                return [2 /*return*/];
            });
        }); }, function (error) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                console.log('error');
                console.log(error);
                return [2 /*return*/];
            });
        }); });
    };
    VerifikasiComponent.prototype.sendData = function (url, data) {
        console.log(url);
        console.log(data);
        var header = { 'Content-Type': 'application/json' };
        return this.http.post(url, data, { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"](header) })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (result) {
            // console.log('httpPost result');
            // console.log(result);
            location.reload();
            return result;
        }));
    };
    VerifikasiComponent.prototype.getDataTPSDetail = function (level, kode, validasi) {
        var _this = this;
        var table_json = [];
        var total1 = 0;
        var total2 = 0;
        var total = 0;
        //  get Data
        this.db.collection('/' + level)
            .valueChanges()
            .subscribe(function (item) {
            console.log(item);
            item.forEach(function (it) {
                var temp = {};
                temp['kode_wilayah'] = it['kode_tps'];
                temp['nama_wilayah'] = it['nama_tps'];
                temp['suara_1'] = it['suara_1'];
                temp['suara_2'] = it['suara_2'];
                temp['suara_sah'] = it['suara_sah'];
                temp['suara_tidaksah'] = it['suara_tidaksah'];
                temp['suara_scan_1'] = it['suara_scan_1'];
                temp['suara_scan_2'] = it['suara_scan_2'];
                temp['suara_scan_3'] = it['suara_scan_3'];
                temp['status_validasi'] = it['status_validasi'];
                temp['suara_total'] = Number(it['suara_1']) + Number(it['suara_2']);
                total1 = total1 + Number(temp['suara_1']);
                total2 = total2 + Number(temp['suara_2']);
                total = Number(total1) + Number(total2);
                table_json.push(temp);
            });
            _this._dataLoaded_detail = true;
            _this._dataTable = table_json;
            _this._dataTableTotal1 = total1;
            _this._dataTableTotal2 = total2;
            _this._dataTableTotal = total;
        });
    };
    VerifikasiComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-verifikasi',
            template: __webpack_require__(/*! ./verifikasi.component.html */ "./src/app/layout/verifikasi/verifikasi.component.html"),
            styles: [__webpack_require__(/*! ./verifikasi.component.scss */ "./src/app/layout/verifikasi/verifikasi.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"]])
    ], VerifikasiComponent);
    return VerifikasiComponent;
}());



/***/ }),

/***/ "./src/app/layout/verifikasi/verifikasi.module.ts":
/*!********************************************************!*\
  !*** ./src/app/layout/verifikasi/verifikasi.module.ts ***!
  \********************************************************/
/*! exports provided: VerifikasiModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifikasiModule", function() { return VerifikasiModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _verifikasi_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./verifikasi-routing.module */ "./src/app/layout/verifikasi/verifikasi-routing.module.ts");
/* harmony import */ var _verifikasi_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./verifikasi.component */ "./src/app/layout/verifikasi/verifikasi.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var VerifikasiModule = /** @class */ (function () {
    function VerifikasiModule() {
    }
    VerifikasiModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _verifikasi_routing_module__WEBPACK_IMPORTED_MODULE_2__["VerifikasiRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]],
            declarations: [_verifikasi_component__WEBPACK_IMPORTED_MODULE_3__["VerifikasiComponent"]]
        })
    ], VerifikasiModule);
    return VerifikasiModule;
}());



/***/ })

}]);
//# sourceMappingURL=verifikasi-verifikasi-module.js.map